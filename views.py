from publish_stream.models import SimpleNewsletterSubscription as Subsc
from publish_stream.forms import SubscriptionForm
from django.views import View
from django.shortcuts import render
from django.utils.timezone import datetime

import logging

log = logging.getLogger(__name__)

TPL_SUBSCRIBE_INDEX = "publish_stream/newsletter/subscribe/index.html"
TPL_SUBSCRIBE_SUCCESS = "publish_stream/newsletter/subscribe/success.html"
TPL_CONFIRM_SUCCESS = "publish_stream/newsletter/confirm/success.html"
TPL_CONFIRM_FAILURE = "publish_stream/newsletter/confirm/failure.html"


class SubscribeEmailView(View):
    def get(self, request):
        form = SubscriptionForm()
        return render(request, TPL_SUBSCRIBE_INDEX, {form: form})

    def post(self, request):
        form = SubscriptionForm(request.POST)
        if form.is_valid():
            return render(request, TPL_SUBSCRIBE_SUCCESS)


class ConfirmSubscriptionView(View):
    def get(self, request, subscription_key):
        subsc = Subsc.objects.get(subscription_key=subscription_key)
        if subsc is None:
            log.warning("Subscription with UUID %s does not exist", subscription_key)
            return render(request, TPL_CONFIRM_FAILURE)
        subsc.confirmed = datetime.now()
        return render(request, TPL_CONFIRM_SUCCESS)
