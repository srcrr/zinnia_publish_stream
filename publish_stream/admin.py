from django.contrib import admin
from django import forms
from zinnia.admin.entry import EntryAdmin
from zinnia.models.entry import Entry
from django.urls import reverse
from django.utils.safestring import mark_safe
from publish_stream.models import (
    PublishedArticleLog,
    ConsoleLogChannel,
    TumblrTextPublishChannel,
    ConsoleLogChannel,
    OauthRestChannelMixin,
    EmailPublishChannel,
    # PublishableEntry,
)


class OauthRestChannelMixinAdmin(admin.ModelAdmin):
    oauth_secret = forms.CharField(widget=forms.PasswordInput)
    consumer_secret = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = OauthRestChannelMixin


class PublishedArticleLogAdmin(admin.ModelAdmin):
    list_display = ("entry", "channel")

    class Meta:
        model = PublishedArticleLog


admin.site.register(TumblrTextPublishChannel, OauthRestChannelMixinAdmin)
admin.site.register(ConsoleLogChannel)
admin.site.register(PublishedArticleLog, PublishedArticleLogAdmin)
admin.site.register(Entry, EntryAdmin)
admin.site.register(EmailPublishChannel)
