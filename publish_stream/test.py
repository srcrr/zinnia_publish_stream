from django import test
from zinnia.models.entry import Entry
from django.contrib.sites.models import Site
from damngood.models import ConsoleLogChannel, PublishedArticleLog
from faker import Faker

fake = Faker()


class TestPublishableEntry(test.TestCase):
    def test_console_channel_draft(self):
        site = Site.objects.create(domain=fake.domain_name(), name="Unit Test Site")
        log_channel = ConsoleLogChannel.objects.create()
        log_channel.sites.add(site)
        log_channel.save()

        entry = Entry.objects.create(
            title=fake.sentence(), content=fake.paragraph(), status=0, slug=fake.word()
        )
        entry.sites.add(site)
        entry.save()

        self.assertFalse(
            PublishedArticleLog.objects.filter(
                entry=entry, channel=log_channel
            ).exists()
        )

    def test_console_channel_hidden(self):
        site = Site.objects.create(domain=fake.domain_name(), name="Unit Test Site")
        log_channel = ConsoleLogChannel.objects.create()
        log_channel.sites.add(site)
        log_channel.save()

        entry = Entry.objects.create(
            title=fake.sentence(), content=fake.paragraph(), status=1, slug=fake.word()
        )
        entry.sites.add(site)
        entry.save()

        self.assertFalse(
            PublishedArticleLog.objects.filter(
                entry=entry, channel=log_channel
            ).exists()
        )

    def test_console_channel_published(self):
        site = Site.objects.create(domain=fake.domain_name(), name="Unit Test Site")
        log_channel = ConsoleLogChannel.objects.create()
        log_channel.sites.add(site)
        log_channel.save()

        entry = Entry.objects.create(
            title=fake.sentence(), content=fake.paragraph(), status=2, slug=fake.word()
        )
        entry.sites.add(site)
        entry.save()

        self.assertTrue(
            PublishedArticleLog.objects.filter(
                entry=entry, channel=log_channel
            ).exists()
        )

    def test_no_republish(self):
        site = Site.objects.create(domain=fake.domain_name(), name="Unit Test Site")
        log_channel = ConsoleLogChannel.objects.create()
        log_channel.sites.add(site)
        log_channel.save()

        entry = Entry.objects.create(
            title=fake.sentence(), content=fake.paragraph(), status=2, slug=fake.word()
        )
        entry.sites.add(site)
        entry.save()
        self.assertEqual(
            PublishedArticleLog.objects.filter(
                entry=entry, channel=log_channel
            ).count(),
            1,
        )

        entry.content = fake.paragraph()
        entry.save()

        self.assertEqual(
            PublishedArticleLog.objects.filter(
                entry=entry, channel=log_channel
            ).count(),
            1,
        )
