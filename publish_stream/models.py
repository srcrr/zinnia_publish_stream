from django.db import models
from django.forms.widgets import PasswordInput
import sys
from zinnia.models_bases import entry
from django.conf import settings
from uuid import uuid4


from django.contrib.sites.models import Site
from zinnia.models_bases.entry import AbstractEntry
from newsletter.models import Subscription, Newsletter

# from zinnia.models.entry import Entry
from polymorphic.models import PolymorphicModel
from django import template
import pytumblr

from django.utils.html import strip_tags
from django.core.mail import send_mail

import logging

log = logging.getLogger(__name__)
# print(f"This log name is '{__name__}'")

DEFAULT_SECRET = dict(max_length=2048)


class PublishedArticleLog(models.Model):
    """ A marker (or a log) to verify that we aren't republishing an article.

    :field entry: The Zinnia entry we're publishing.

    :field publish_date: The date that the publication was pushed to other source.

    :field channel: The publication channel.
    """

    entry = models.ForeignKey("zinnia.Entry", on_delete=models.CASCADE)
    publish_date = models.DateTimeField(auto_now=True)
    channel = models.ForeignKey("PublicationChannel", on_delete=models.CASCADE)


class PublicationChannel(PolymorphicModel):
    """ Represents a place to repost an entry.

    :field sites: For any entry that's published to a site, these will trigger
    the PublicationChannel to publish.

    :minimum_status: The minimum `zinnia` entry status that an article must
    be to be published (default is `published`)
    """

    enabled = models.BooleanField(default=True)
    sites = models.ManyToManyField(Site, related_name="sites")
    minimum_status = models.IntegerField(
        default=2,
        choices=AbstractEntry.STATUS_CHOICES,
        help_text="This channel will not publish unless an article status is at least this value.",
    )

    def publish(self, entry):
        """ Goes through the effort of publishing the article.

        Note that this will only execute if the (entry, channel) pair is not
        found in PublishedArticleLog. Therefore, if you want to force a
        re-publish, make sure the (entry, channel) pair is not in
        PublishedArticleLog

        Must be implemented by subclasses. Raises a `NotImplementedError`
        otherwise.

        :param entry: The entry to publish.
        """
        raise NotImplementedError()

    def try_publish(self, entry):
        """ Publishes the entry if it isn't already published. """
        log.debug("Publising to %s", self)
        if PublishedArticleLog.objects.filter(entry=entry, channel=self).exists():
            return
        if entry.status < self.minimum_status:
            return
        self.publish(entry)
        PublishedArticleLog.objects.create(entry=entry, channel=self)


class ConsoleLogChannel(PublicationChannel):
    """A very simple console logging channel. Mainly used for tests."""

    def publish(self, entry):
        log.info("=" * 10)
        log.info("Entry published")
        log.info("-" * 10)
        log.info("Title: %s", entry.title)
        log.info("Content:")
        log.info("%s", entry.html_content)
        log.info("-" * 10)


class OauthRestChannelMixin(models.Model):
    """Any channel that uses Oauth authentication. """

    consumer_key = models.CharField(max_length=2048)
    consumer_secret = models.CharField(**DEFAULT_SECRET)
    oauth_token = models.CharField(max_length=2048)
    oauth_secret = models.CharField(**DEFAULT_SECRET)

    class Meta:
        abstract = True


class TumblrTextPublishChannel(PublicationChannel, OauthRestChannelMixin):
    """ Publishes to Tumblr.

    :field blog_name: The blog name to publish to. If not given, then this
    channel will attempt to discover it with client info and choose the
    first blog name.

    :field default_state: Where to post articles. Default is in the queue.

    """

    DEFAULT_PUBLISH_STATES = (
        ("published", "published"),
        ("draft", "draft"),
        ("queue", "queue"),
        ("private", "private"),
    )

    blog_name = models.CharField(max_length=1024, blank=True, null=True)

    default_state = models.CharField(
        max_length=20, choices=DEFAULT_PUBLISH_STATES, default="queue"
    )

    def get_client(self):
        return pytumblr.TumblrRestClient(
            self.consumer_key, self.consumer_secret, self.oauth_token, self.oauth_secret
        )

    def publish(self, entry):
        client = self.get_client()
        info = client.info()
        first_blog_name = None
        try:
            first_blog_name = info["user"]["blogs"][0]["name"]
        except Error as e:
            if not self.blog_name:
                raise RuntimeError(
                    "could not get tumblr account info. blog_name must be set."
                )
        body = template.loader.render_to_string(
            "publish_stream/tumblr.html", {"entry": entry,}
        )
        print(body)
        text_post = client.create_text(
            self.blog_name or first_blog_name,
            state=self.default_state,
            slug=entry.slug,
            title=entry.title,
            body=body,
        )
        log.debug("Response from tumblr: %s", text_post)


class EmailPublishChannel(PublicationChannel):
    from_email = models.EmailField()
    newsletter = models.ForeignKey(Newsletter, on_delete=models.CASCADE)

    def __str__(self):
        return f"EmailPublishChannel from <{self.from_email}>"

    def publish(self, entry):
        emails = [
            subscription.email
            for subscription in self.newsletter.subscription_set.filter(
                subscribed=True
            ).all()
        ]
        _rndr = template.loader.render_to_string
        log.info("Sending newsletter email: to %d recipients", len(emails))
        for email in emails:
            context = {
                "entry": entry,
                "newsletter": self.newsletter,
                "email": email,
            }
            subject = _rndr("publish_stream/email/subject.txt", context).strip()
            html_message = _rndr(
                "publish_stream/email/html_message.html", context
            ).strip()
            message = _rndr("publish_stream/email/text_message.txt", context).strip()
            from_email = settings.EMAIL_HOST_USER
            send_mail(subject, message, from_email, [email,], html_message=html_message)


class PublishableEntry(AbstractEntry):
    """Nothing different from a normal entry except that on a save, the
    PublishChannels are triggered."""

    def save(self, *args, **kwargs):
        super(PublishableEntry, self).save(*args, **kwargs)
        log.debug("Sites: %s", [site.id for site in self.sites.all()])
        channels = PublicationChannel.objects.filter(
            sites__id__in=[site.id for site in self.sites.all()], enabled=True
        )
        for channel in channels.all():
            channel.try_publish(self)

    class Meta(AbstractEntry.Meta):
        abstract = True
