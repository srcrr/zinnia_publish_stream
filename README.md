# Zinnia Publish Stream

A plugin for Django Zinnia that automagically publishes your entries to other
platforms.

When an article is published, this system is triggered to publish an announcement
(or the article itself) to a variety of different platforms.

Currently the only channel is Tumblr.

But...this is extensible so if a Publish Channel doesn't exist, you can create
one!

# Installation

Dependencies
------------

Make sure to install these packages prior to installation :

-   [Python](http://www.python.org/) \>= 3.5
-   [Django](https://www.djangoproject.com/) \>=
    2.2
-   [Pillow](http://python-imaging.github.io/Pillow/) \>= 7.0.0
-   [django-mptt](https://github.com/django-mptt/django-mptt/) \>= 0.11.0
-   [django-tagging](https://code.google.com/p/django-tagging/) \>= 0.5.0
-   [beautifulsoup4](http://www.crummy.com/software/BeautifulSoup/) \>= 4.8.2
-   [mots-vides](https://github.com/Fantomas42/mots-vides) \>= 2015.5.11
-   [pyparsing](http://pyparsing.wikispaces.com/) \>= 2.4.6
-   [regex](https://pypi.python.org/pypi/regex) \>= 2020.2.20
-   [django-contrib-comments](https://github.com/django/django-contrib-comments) \>= 1.9.2

The packages below are optionnal but needed for run the full test suite
or migrate the database.

-   [django-xmlrpc](https://github.com/Fantomas42/django-xmlrpc) \>= 0.1.8

Note that all the needed dependencies will be resolved if you install
zinnia_publish_stream with **pip** or **easy\_install**, excepting Django.

Getting the code
----------------

For the latest stable version of zinnia_publish_stream use **easy\_install**:

    $ easy_install zinnia-publish-stream

or use **pip**:

    $ pip install zinnia-publish-stream

You could also retrieve the last sources from
<https://gitlab.com/srcrr/zinnia-publish-stream.git>. Clone the repository
using **git** and run the installation script:

    $ git clone git://gitlab.com/srcrr/zinnia-publish-stream.git
    $ cd zinnia_publish_stream
    $ python setup.py install

or more easily via **pip**:

    $ pip install -e git://gitlab.com/srcrr/zinnia-publish-stream.git#egg=zinnia-publish-stream

Applications
------------

Assuming that you have an already existing Django project, register
[`zinnia_publish_stream`], and these
following applications in the [<tt>INSTALLED_APPS</tt>](https://django.readthedocs.io/en/latest/ref/settings.html#std:setting-INSTALLED_APPS "(in Django v3.2)") section of your project's settings.

    INSTALLED_APPS = (
      'django.contrib.auth',
      'django.contrib.admin',
      'django.contrib.sites',
      'django.contrib.sessions',
      'django.contrib.messages',
      'django.contrib.staticfiles',
      'django.contrib.contenttypes',
      'django_comments',
      'mptt',
      'tagging',
      'zinnia',
      'zinnia_publish_stream',
    )

Base Entry Model
----------------

**IMPORTANT**. Ensure that you set `ZINNIA_ENTRY_BASE_MODEL` in settings.

    ZINNIA_ENTRY_BASE_MODEL = "publish_stream.models.PublishableEntry"

The cool part is that if you add your own PublishChannels, the PublishableEntry
will pick it up!

Defining Your Own PublishChannel
--------------------------------

A `PublishChannel` is a django model. Simply extend it, and provide a
`publish` function that publishes an entry.

Here's the `ConsoleLogChannel` as an example:

    class ConsoleLogChannel(PublicationChannel):
        """A very simple console logging channel. Mainly used for tests."""

        def publish(self, entry):
            log.info("=" * 10)
            log.info("Entry published")
            log.info("-" * 10)
            log.info("Title: %s", entry.title)
            log.info("Content:")
            log.info("%s", entry.html_content)
            log.info("-" * 10)

When an Entry is published
--------------------------

When an entry has been published to an EntryChannel, it's recorded in
the `PublishedArticleLog` model so that it won't be published again.

If you want to force a re-publish, delete the (Entry, Channel) pair
out of the `PublishedArticleLog` model.
